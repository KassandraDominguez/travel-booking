# travel-booking



## Overview

The Travel Booking Application is a Spring Boot-based web application that allows users to search for and book flights, hotels, and rental cars. It integrates with third-party services to fetch real-time availability and pricing information. This README provides an overview of the project, how to set it up, and its key features.

## Features

- Search for flights by origin, destination, and date.
- Search for hotels by location and price range.
- Search for rental cars.
- User authentication and authorization.
- Integration with third-party services for real-time data.
- CRUD operations for flights, hotels, and rental cars.

## Technologies Used

- Java
- Spring Boot
- Spring Data JPA
- Spring Security
- RESTful API


## Setup

1. Clone the repository
2. Configure the application properties:
- Configure your database settings, API keys, and other properties in `application.properties`.
3. Build and run the application
4. Access the application in your web browser: http://localhost:8080 (or the configured port).




## Contributing

Contributions are welcome! Please follow the [Contributing Guidelines](CONTRIBUTING.md) to get started.

## License

This project is licensed under the [MIT License](LICENSE.md).

## Contact

If you have any questions or need further assistance, please contact Kassandra Dominguez

## Feedback
- Was it easy to complete the task using AI? 
it was easy to have the basic structure but it is not for more specific and customs things

- How long did task take you to complete? (Please be honest, we need it to gather anonymized statistics) 
it took about 4hrs and is not ready yet.
- Was the code ready to run after generation? What did you have to change to make it usable?
No, the code was nor ready to run imediately
- Which challenges did you face during completion of the task?
the third-party integracion is an complex activity that requires much more time

- Which specific prompts you learned as a good practice to complete the task?
it is better to explain the contentx of the application.




